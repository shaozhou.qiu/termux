# test server status
testServer(){
    ip=$1;
    port=$2;
    ping -c 3 -i 0.2 -W 3 $ip &> /dev/null;
    if [ $? -eq 0 ];then
        ret=`nmap $ip -p $port --open|grep $port`;
	if [ "$ret" == "" ]; then
	    return -1;
	else
	    return 0;
	fi
    fi
    #echo "$ip:$port";
    return -1;
}

# back local data to remote
backupCommonData(){
    ip=$1;
    port=$2;
    r_usr=$3;
    r_pwd=$4;
    r_folder=$5;
    l_basedir=/storage/emulated/0;
    items=(DCIM Download Movies Music Pictures Sounds);
    for v in ${items[@]}; do
	    ldir=$l_basedir/$v;
        echo "rsync copy $ldir...";
        backupData $ip $port $r_usr $r_pwd $r_folder $ldir;
    done
    # find files
    fexts=(vcf);
    r_folders=(contact);
    n=-1;
    for ext in ${fexts[@]}; do
        n=$(expr $n \* 1 + 1);
        fs=`find /storage -name "*.$ext"`;
        for f in  ${fs[@]}; do
            echo "rsync copy $f...";
            backupData $ip $port $r_usr $r_pwd $r_folder/${r_folders[$n]}/ $f;
        done
    done

    # backup tencent
    tencent=$l_basedir/Tencent;
    weixin=$tencent/MicroMsg;
    if [ ! -d $weixin ]; then
        echo "rsync copy $weixin...";
        backupData $ip $port $r_usr $r_pwd $r_folder/Pictures/WeiXin $weixin/WeiXin/;
        backupData $ip $port $r_usr $r_pwd $r_folder/Download/WeiXin $weixin/Download/;
    fi
    if [ ! -d $tencent ]; then
        echo "rsync copy $tencent...";
        backupData $ip $port $r_usr $r_pwd $r_folder/Pictures/QQ $tencent/QQ_Images/;
    fi

    tencentQQ=$l_basedir/Android/data/comtencent.mobileqq/Tencent;
    if [ ! -d $tencentQQ ]; then
        echo "rsync copy $tencentQQ...";
        backupData $ip $port $r_usr $r_pwd $r_folder/Download/QQ $tencentQQ/QQfile_recv/;
    fi
    
}
backupData(){
    ip=$1;
    port=$2;
    r_usr=$3;
    r_pwd=$4;
    r_folders=$5;
    r_basedir=/mnt/sdb1;
    l_dir=$6;
    r_dir=$r_usr@$ip:$r_basedir/$r_folders;

    rsyncopt=(-p $r_pwd rsync -rvt --chmod 777 --exclude ".[a-z0-9]*" --exclude "*/.[a-z0-9]*");
    rsyncopt[${#rsyncopt[@]}]="-e";
    rsyncopt[${#rsyncopt[@]}]="ssh -p $port -o 'StrictHostKeyChecking no'";

    echo "rsync local data to remote[$ip:$port]...";
    sshpass "${rsyncopt[@]}" $l_dir $r_dir;
    #echo "sshpass ${rsyncopt[@]} $l_dir $r_dir"
    #exit 0;
}

main(){
    ip=$1
    port=$2
    r_devname=$3
    r_usr=$4;
    r_pwd=$5;
    testServer $ip $port;
    if [ $? -eq 0 ]; then
        backupCommonData $ip $port $r_usr $r_pwd $r_devname;
	exit 0;
    fi
}
chkRuntime(){
    cmds=(rsync nmap sshpass);
    for cmd in ${cmds[@]}; do
	if ! [ -x "$(command -v $cmd)" ]; then
            pkg install $cmd -y;
	else
	    echo "$cmd .......... ok"
	fi
    done
}

#deviceName=P30;
#rusr=root;
#rpwd=toor;
#main 10.0.0.8 22 $deviceName $rusr $rpwd;
#main frp4.39doo.com 42201 $deviceName $rusr $rpwd;

chkRuntime;
termux-setup-storage

