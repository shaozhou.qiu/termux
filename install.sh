installService(){
    #appsh=`pwd`/bin/service.sh;
    svrsh=$1;
    atsh=$PREFIX/etc/bash.bashrc;
    isExist=`cat $atsh | grep $svrsh`;
    if [ "$isExist" == "" ]; then
        echo $svrsh >> $atsh;
    fi
}
createService(){
    appn=$1;
    basePath=`pwd`;
    srcPath=$basePath/src/$appn;
    dstPath=$basePath/bin/$appn;
    if [ -f "$dstPath" ]; then
        rm $dstPath -rf;
    fi
    if [ -f "$srcPath" ]; then
        cp $srcPath $dstPath;
    fi
    if [ -f "$dstPath" ]; then
        chmod 777 $dstPath;
        installService $dstPath;
    fi

}
createService service.sh;

#upgrade app
appsh=`pwd`/src/upgrade.sh;
echo "cd `pwd`" > $appsh;
echo "./upgrade.sh" >> $appsh;
createService upgrade.sh;
rm $appsh -rf;


createService backup.sh;


read -p "Enter ssh user:" rusr;
read -p "Enter password with user:" rpwd;
read -p "Enter device name:" devName;

conf=`pwd`/conf/global.conf;
echo "s_usr=$rusr;" >$conf;
echo "s_pwd=$rpwd;" >>$conf;
echo "devn=$devName;" >>$conf;


# create autorun service
appsh=`pwd`/bin/service.sh;
echo "#!/bin/bash" > $appsh;
echo "source `pwd`/src/service.sh" >> $appsh;
echo "openApp sshd" >> $appsh;
chmod 777 $appsh;

# create backup
appsh=`pwd`/bin/backup.sh;
echo "#!/bin/bash" > $appsh;
echo "source `pwd`/src/backup.sh" >> $appsh;
echo "source $conf" >> $appsh;

ip=10.0.0.8; port=22;
echo "echo 'Verify that the server($ip:$port) is availabel.';" >> $appsh;
echo "main $ip $port \$devn \$s_usr \$s_pwd;" >> $appsh;

num=(1 2 3 4 5 6 7 8 9);
for i in ${num[@]}; do
    ip=frp$i.39doo.com; port=42201;
    echo "echo 'Verify that the server($ip:$port) is availabel.';" >> $appsh;
    echo "main $ip $port \$devn \$s_usr \$s_pwd;" >> $appsh;
done
chmod 777 $appsh;



echo "finish!";
